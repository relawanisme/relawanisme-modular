package com.relawanisme.utils.business

import android.os.Looper

fun onMainThread() =  Looper.myLooper() == Looper.getMainLooper()