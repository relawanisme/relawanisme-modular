package com.relawanisme.android.application.network.response
import com.google.gson.annotations.SerializedName


data class LoginRes(
    @SerializedName("id")
    val id: String?,
    @SerializedName("fullname")
    val fullname: String?,
    @SerializedName("email")
    val email: String?
)