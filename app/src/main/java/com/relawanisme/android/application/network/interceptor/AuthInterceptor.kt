package com.relawanisme.android.application.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val req = chain.request()
//        val url = req.url.newBuilder().addQueryParameter("apikey", BuildConfig.API_KEY).build()
//        req = req.newBuilder().url(url).build()
        return chain.proceed(req)
    }
}