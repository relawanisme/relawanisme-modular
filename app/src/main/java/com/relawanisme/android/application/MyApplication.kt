package com.relawanisme.android.application

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.relawanisme.android.di.feature.authModule
import com.relawanisme.android.di.networkModule
import com.relawanisme.android.di.prefModule
import org.koin.android.ext.koin.androidContext
import timber.log.Timber
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        // Doing inject your module or some dependencies in here
        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            modules(
                listOf(
                    prefModule,
                    networkModule,
                    authModule
                )
            )
        }
        Timber.plant(Timber.DebugTree())
        AndroidThreeTen.init(this)
    }

}