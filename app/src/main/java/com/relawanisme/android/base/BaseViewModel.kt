package com.relawanisme.android.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

/**
 * Created by Dimas Prakoso on 31,January,2020
 */
abstract class BaseViewModel: ViewModel() {

    fun launch(blockScope: suspend CoroutineScope.() -> Unit) =
        viewModelScope.launch(block = blockScope)

    fun async(blockScope: suspend CoroutineScope.() -> Unit) =
        viewModelScope.async(block = blockScope)
}