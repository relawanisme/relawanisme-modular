package com.relawanisme.android.base

import android.app.Activity
import androidx.appcompat.widget.Toolbar

/**
 * Created by Dimas Prakoso on 07/01/2020.
 */
interface BaseView {

    fun setupToolbar(toolbar: Toolbar?, title: String, isChild: Boolean)

    fun showMessage(message: String)

}