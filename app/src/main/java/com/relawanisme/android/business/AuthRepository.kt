package com.relawanisme.android.business

import com.relawanisme.android.application.model.User
import com.relawanisme.android.base.BaseRepository
import com.relawanisme.android.data.api.AuthApi
import com.relawanisme.android.data.preference.AppPreference
import com.relawanisme.utils.business.Resource
import com.relawanisme.utils.business.helpers.DataFetchHelper
import retrofit2.Response

class AuthRepository(
    private val authAPi: AuthApi,
    private val appPreference: AppPreference
) : BaseRepository(), AuthDataSource {

    override suspend fun login(): Resource<User> {
        val dataFetchHelper = object : DataFetchHelper.NetworkOnly<User>("Login"){
            override suspend fun getDataFromNetwork(): Response<out Any?> {
                return authAPi.login()
            }

            override suspend fun convertApiResponseToData(response: Response<out Any?>): User {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }
        return dataFetchHelper.fetchDataIOAsync().await()
    }
}