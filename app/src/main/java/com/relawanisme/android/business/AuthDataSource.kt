package com.relawanisme.android.business

import com.relawanisme.android.application.model.User
import com.relawanisme.utils.business.Resource

interface AuthDataSource {

    suspend fun login(): Resource<User>
}