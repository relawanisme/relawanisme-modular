package com.relawanisme.android.data.api

import com.relawanisme.android.application.network.response.LoginRes
import retrofit2.Response
import retrofit2.http.POST

interface AuthApi {

    @POST("login")
    suspend fun login(): Response<LoginRes>

}