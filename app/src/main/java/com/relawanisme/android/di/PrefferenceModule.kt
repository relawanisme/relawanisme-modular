package com.relawanisme.android.di

import com.relawanisme.android.data.preference.AppPreference
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val prefModule = module {
    single { AppPreference(androidContext()) }
}