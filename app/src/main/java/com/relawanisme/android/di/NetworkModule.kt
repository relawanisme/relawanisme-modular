package com.relawanisme.android.di

import com.relawanisme.android.BuildConfig
import com.relawanisme.android.application.network.interceptor.AuthInterceptor
import com.relawanisme.utils.constant.NETWORKS
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import java.util.concurrent.TimeUnit

val networkModule = module {
    single { AuthInterceptor() }
    single { provideLoggingInterceptor(BuildConfig.DEBUG) }
    single { provideOkHttpClient(get(), get()) }
}

fun provideOkHttpClient(authInterceptor: AuthInterceptor, loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder()
        .connectTimeout(NETWORKS.NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(NETWORKS.NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(NETWORKS.NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(authInterceptor)
        .addInterceptor(loggingInterceptor)
        .build()
}

fun provideLoggingInterceptor(isShow: Boolean): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()
    logger.level = if (isShow) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    return logger
}