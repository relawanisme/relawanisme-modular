package com.relawanisme.android.di.feature

import com.relawanisme.android.BuildConfig
import com.relawanisme.android.business.AuthDataSource
import com.relawanisme.android.business.AuthRepository
import com.relawanisme.android.data.api.AuthApi
import com.relawanisme.utils.network.ApiService
import org.koin.dsl.module

val authModule = module {
    single { ApiService.createService(AuthApi::class.java, get(), BuildConfig.BASE_URL) }

    single<AuthDataSource> { AuthRepository(get(), get()) }
}
